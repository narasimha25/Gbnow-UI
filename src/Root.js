import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxPromise from 'redux-promise';
import reducers from 'reducers';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

export default ({ children, initialState = {} }) => {

  const client = new ApolloClient({
    uri: 'https://w5xlvm3vzz.lp.gql.zone/graphql'
  });

  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(reduxPromise)
  );

  return <Provider store={store}>
          <ApolloProvider client={client}>
            {children}
          </ApolloProvider>
        </Provider>

};
