import React from 'react';
import ReactDOM from 'react-dom';
import App from 'App';

import { mount } from 'enzyme';

import Root from 'Root';

let wrapped;

it('renders without crashing', () => {
  wrapped = mount(
    <Root>
      <App />
    </Root>
  );

  wrapped.unmount();
});
