import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import appStore from 'utilities/store';


import indexRoutes from 'routes';


const browserHistory = createBrowserHistory();

ReactDOM.render(
  <Provider store={appStore}>
      <Router history={browserHistory}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
      </Router>
  </Provider>
  , document.getElementById('root'));
