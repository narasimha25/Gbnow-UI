import React, { Component } from 'react';
import PlanMembers from 'components/claim/PlanMembers';

class App extends Component {
  render() {
    return (
      <div>
        <PlanMembers />
      </div>
    );
  }
}

export default App;
