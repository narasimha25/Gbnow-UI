import { combineReducers } from "redux";

// Reducers import
import {
  appCurrencies,
  Todos
} from 'components/claim/Reducers';

const appReducer =  combineReducers({
  appCurrencies,
  Todos
});

export default appReducer;
