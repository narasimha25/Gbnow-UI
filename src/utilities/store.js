import {  createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import appReducers from 'utilities/Reducers';
import { createLogger } from 'redux-logger';

import { callApollo, callRest } from 'components/claim/Actions';

// Create Saga Middleware
const sagaMiddleware = createSagaMiddleware();

// Create the redux logger
const appLogger = createLogger({
  collapsed: true,
  duration: true
});

const store = createStore(
  appReducers,
  applyMiddleware(appLogger, sagaMiddleware)
);
console.log('calling saga action');
sagaMiddleware.run(callApollo);
sagaMiddleware.run(callRest);

export default store;
