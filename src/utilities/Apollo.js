import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { RestLink } from 'apollo-link-rest';
import ApolloClient from 'apollo-client';
import { gql } from 'apollo-boost';

const cache =  new InMemoryCache().restore(window.__APOLLO_STATE__);

const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: 'https://w5xlvm3vzz.lp.gql.zone/graphql'
  }),
  cache
});

// Queries
const getCurrencies = gql`
  {
    rates(currency: "USD") {
      currency
    }
  }
`;

const RestQuery = gql`
  query RestData {
    todos @rest(type: "[Todo]", path: "todos"){
      idea
      title
    }
  }
`;

// const query = gql`
//   query luke {
//     person @rest(type: "Person", path: "people/1/") {
//       name
//     }
//   }
// `;

const apolloRestClient = new ApolloClient({
  link: new RestLink({
    uri: 'https://jsonplaceholder.typicode.com/'
  }),
  cache
});

export { apolloClient, getCurrencies, apolloRestClient, RestQuery };
