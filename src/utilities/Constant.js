export const GET_CURRENCIES = 'get_currencies';
export const GET_CURRENCIES_ERROR = 'get_currencies_error';
export const GET_TODOS = 'get_todos';
export const GET_TODOS_ERROR = 'get_todos_error';
