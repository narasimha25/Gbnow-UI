import React, { Component } from 'react';

export default ChildComponent => {
  class ComposedComponent extends Component {

    CheckAuth(){
      // Check for the authentication based on the session
      // If auth not present then force redirection
      // If Auth is present, it will extract the user information and store them localy

    }

    getCurrentUsername(){
      return 'User 1';
    }

    getCurrectContractNumber(){
      return 'C3728937892';
    }

    getCurrentPolicyNumber(){
      return 'P743829044389';
    }

    componentDidMount(){
      this.CheckAuth();
    }

    componentDidUpdate(){
      this.CheckAuth();
    }

    render(){
      return (
        <ChildComponent
          Username={this.getCurrentUsername()}
          PolicyNumber={this.getCurrentPolicyNumber()}
          ContractNumber={this.getCurrectContractNumber()}
          {...this.props}
        />
      );
    }
  }

  return ComposedComponent;
};
