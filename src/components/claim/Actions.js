import { put } from "redux-saga/effects";
import { apolloClient, getCurrencies, apolloRestClient, RestQuery } from 'utilities/Apollo';
import {
  GET_CURRENCIES,
  GET_CURRENCIES_ERROR,
  GET_TODOS,
  GET_TODOS_ERROR
} from 'utilities/Constant';

function *callApollo(){
  try {
    const currencies = yield apolloClient.query({
      query: getCurrencies,
    });
    yield put({type: GET_CURRENCIES, payload: currencies.data.rates});
  } catch (e) {
    yield put({type: GET_CURRENCIES_ERROR, payload: e.message});
  }
}
function *callRest(){
  try {
    console.log('calling rest query');
    const todos = yield apolloRestClient.query({
      query: RestQuery,
    });
    console.log(todos);
    yield put({type: GET_TODOS, payload: todos.data.todos});
  } catch (e) {
    console.log('ERROR');
    console.log(e);
    yield put({type: GET_TODOS_ERROR, payload: e.message});
  }
}

export { callApollo, callRest };
