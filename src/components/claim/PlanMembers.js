import React, { Component } from 'react';
import {connect} from "react-redux";

class PlanMembers extends Component {

  handelError(hasError, error){
    if(hasError){
      console.log(error);
      return <div>An error occured {error}</div>
    }
  }

  render(){
    return (
      <div>
        <h1>GraphQL</h1>
        { this.handelError(this.props.hasError, this.props.error) }
        <div>Plan members, retrieved currencies: { this.props.currencies.length }</div>
        <h1>REST</h1>
        <div>Plan members, retrieved todos: { this.props.todos.length }</div>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    currencies: store.appCurrencies.currencies,
    hasError: store.appCurrencies.hasError,
    error: store.appCurrencies.error,
    todos: store.Todos.todos
  };
};

export default connect(mapStateToProps)(PlanMembers);
