import {
  GET_CURRENCIES,
  GET_CURRENCIES_ERROR,
  GET_TODOS,
  GET_TODOS_ERROR
} from 'utilities/Constant';


export function Todos (state = { todos: [], hasError: false, error: '' }, action) {
  switch (action.type) {
    case GET_TODOS:
      return { ...state, todos: action.payload };
    case GET_TODOS_ERROR:
      return { ...state, hasError: true, error: action.payload };
    default:
      return state;
  }
};

export function appCurrencies (state = { currencies: [], hasError: false, error: '' }, action) {
  switch (action.type) {
    case GET_CURRENCIES:
      return { ...state, currencies: action.payload };
    case GET_CURRENCIES_ERROR:
      return { ...state, hasError: true, error: action.payload };
    default:
      return state;
  }
};
