This project is part of Manulife | GB NOW - New Claims Portal - 10912

## Table of contents

- [Project Description](#project-description)
- [Obtaining the Source Code](#obtaining-the-source-code)
- [Running the Project](#linked-jira-artifacts)
- [Running Tests](#running-tests)
- [ESLint Code Quality Checks](#eslint-code-quality-checks)
- [SonarQube Setup](#sonarqube-setup)
  - [Requirements](#requirements)
  - [SonarQube Server Setup](#sonarqube-server-setup)
  - [SonarQube Scanner Setup](#sonarqube-scanner-setup)
  - [Project Configuration](#project-configuration)
- [Dependencies](#dependencies)
  - [Production Packages](#production-packages)
  - [Development Dependencies](#development-dependencies)
  - [Other Dependencies](#other-dependencies)
- [Related Links](#related-links)
- [Other Info](#other-info)

## Project description

This project is the front end UI for (Manulife | GB NOW - New Claims Portal - Project ID 10912).

## Obtaining the Source Code

The latest source code of the project can be pulled from [the project's git repository](https://gitlab.com/balghazi/sample-app).

## Running the Project

After fetching the project files, execute `npm install` in order to get
all the package dependencies for the project then the following files need
to be modified with the correct values depending on the environment:

```
  Provide list of configuration files and the values that need to be changed.
  Ex: Back end API endpoints, authentication keys... etc.
```

Once the above files are modified, the following run scripts are available:

- `npm start`: start the application in development mode. The application
  can be accessed from [http://localhost:3000](http://localhost:3000).
- `npm run build`: Build the app and write the output to the `build` folder.


## Running Tests

All test files are placed under the __\_\_tests\_\___ subfolder of each component.

To run the tests execute `npm test`
To generate the test coverage report execute `npm test -- --coverage`

## ESLint Code Quality Checks

The project has ESLint configured to perform code quality checks, available
start scripts:

- `npm run elint`: Run the ESlint scanner and provide detailed results.
- `npm run elint:summary`: Run ESlint scanner and provide a formatted summary
    of the scan result.
- `npm run elint:table`: Run the ESlint scanner and provide a table view of
    the results.

## SonarQube Setup

### Requirements

In order to run the SonarQube analysis correctly, ESLint should be configured correctly. The
ESLint configurations recommended for React are available in the file .eslintrc on the project root.

### SonarQube Server Setup

SonarQube Server can be run locally from the [Official Sonar Cube Container Image](https://hub.docker.com/_/sonarqube/).

```
// Pull the container image from docker hub
docker pull sonarqube
// Run the docker container from the pulled image
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

This will run the server on port 9000, to access the server web interface, browse to the
[SonarQube Dashboard](http://localhost:9000/projects) from 'http://localhost:9000/' and login with the default
login id `admin` and default password `admin`.

Once up and running, add the package for [EsLint scanning](https://github.com/sleroy/SonarEsLintPlugin)
by following the the instructions in [this article](https://medium.com/@slryit/sonarqube-and-reactjs-the-right-way-5979d02c080d).

Create a new project from the admin section, and get the required project key and
login token. Those are required for configuring and running the SonarQube Scanner.

At this point the SonarQube Server is ready.

### SonarQube Scanner Setup

Once the server is up and running, the client scanner needs to be installed, details about the
installation can be found on the [SonarQube website](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner).

- Download the scanner suitable for OS:
[Windows Scanner](https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-windows.zip)
- [MacOS Scanner](https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-macosx.zip)

After downloading and uncompressing the files, add the *bin* folder from the installation directory to the
PATH environment variable.

### Project Configuration

The configuration of SonarQube is provided in the `sonar-project.properties` file,
the following configurations should be modified depending on the environment:

- `sonar.projectKey`: the key of the project in SonarQube.
- `sonar.host.url`: URL of SonaqRube server.

After setting the correct values in the
`sonar-project.properties` file, follow the following instructions to perform
the analysis:

- In a terminal window, go to the project directory.
- Run `npm test -- --coverage` to generate the test coverage `lcov.info` file.
- Run the command `sonar-scanner -Dsonar.sources=. -Dsonar.login=[SONARQUBE-LOGIN-KEY]`
where `[SONARQUBE-LOGIN-KEY]` is the login key obtained from the SonarQube administration
section.
- Once the analisys finishes, the results can be viewed from the project page
on SonarQube web interface.

## Dependencies

### Production Packages

| Package                                       | Category         | Description                                       |
|-----------------------------------------------|------------------|---------------------------------------------------|
| react-helmet                                  | Utility          | HTML header management                            |
| enzyme<br/> enzyme-adapter-react-16                | Testing                 | Functional and UI Unit tests |
| graphql<br/> graphql-tag<br/> react-apollo<br/> apollo-boost | Data Access      | GraphQL integration                               |
| axios                                         | HTTP             | REST service calls                                |
| react-redux<br/> redux-saga<br/> redux-promise          | State Management | Application level state management and middleware |
| react-router-dom                              | Routing          | Handle browser path routing                       |
| @material-ui/core jquery                      | UI               | Material UI / transitions / DOM                   |

### Development Dependencies

| Package | Category | Description |
|-------------------------------------------------------------------|----------|-----------------------------------|
| eslint<br/>  eslint-friendly-formatter<br/>  eslint-plugin-react  | Dev | ESLint code quality checker tools |

### Other Dependencies

- [Google Fonts](https://fonts.google.com/)
- [Google Material Design Icons](https://github.com/google/material-design-icons)

## Related Links

- [Jira Project Workspace](https://photon.atlassian.net/projects/MGB/summary)
- [Document Workspace](https://photon.atlassian.net/wiki/spaces/MGB/pages/482377729/Engineering)
- [SonarQube Report](http://localhost:9000/dashboard?id=photon-template)

## Other Info
